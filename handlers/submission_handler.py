import sys
import random

from math import atan, pi, radians, cos, sin, asin, sqrt

import pymongo

from tornado import gen
from protobuf_to_dict import protobuf_to_dict, dict_to_protobuf

from handlers.base_handler import BaseHandler
from model.location_pb2 import Location, History, RATING_POSITIVE, RATING_NEUTRAL, RATING_NEGATIVE

class SubmissionHandler(BaseHandler):

    # map protobuf enum (int) to field name (string)
    combined_rating_enum = {RATING_POSITIVE: 'positive', RATING_NEUTRAL: 'neutral', RATING_NEGATIVE: 'negative'}

    def initialize(self, db):
        super(SubmissionHandler, self).initialize(db=db)
        # return json of new location lid
        self.set_header('Content-Type', 'application/json')


    @gen.coroutine
    def store_loc_history(self, loc):
        hist = History()
        hist.loc_id = loc.lid
        hist.version = loc.version
        hist.description = loc.description
        hist.title = loc.title
        hist.client_id = loc.client_id
        if loc.HasField('user_rating'):
            hist.rating = loc.user_rating
        yield self.loc_hist_coll.insert(protobuf_to_dict(hist))

    def compute_rating(self, combined_rating):
        pos = combined_rating.positive
        neu = combined_rating.neutral
        neg = combined_rating.negative
        count = pos + neu + neg

        if (count):
            # Between 0 and 1, Higher counts have higher absolute value
            scale_factor = atan(count) * 2 / pi
            return (pos - neg) / count * scale_factor
        return 0

    # Ensures the location is in Manhattan
    def validate_location(self, coordinates):
        # center of NYC is around la guardia
        center_lat = 40.760861
        center_lon = -73.888095
        r_max = 30 # radius in km
        assert self.haversine(center_lat, center_lon, coordinates[1], coordinates[0]) < r_max, 'location is not in NYC'

    def haversine(self, lat1, lon1, lat2, lon2):
        """
        Calculate the great circle distance between two points 
        on the earth (specified in decimal degrees)
        """
        # convert decimal degrees to radians 
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

        # haversine formula 
        dlon = lon2 - lon1 
        dlat = lat2 - lat1 
        a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
        c = 2 * asin(sqrt(a)) 
        r = 6371 # Radius of earth in kilometers. Use 3956 for miles
        return c * r

    @gen.coroutine
    def post(self):
        body = self.request.body
        location = Location()
        try:
            location.ParseFromString(body)
            self.validate_location(location.loc.coordinates)

            query = { 'loc': {
                '$nearSphere': {
                    '$geometry': {'type': 'Point',
                        'coordinates': [float(location.loc.coordinates[0]), float(location.loc.coordinates[1])]
                        },
                    }
                }
            }

            res = yield self.loc_info_coll.find_one(query)

            isNewLocation = False

            existingLoc = Location()
            if res:
                # location already exists
                existingLoc = dict_to_protobuf(Location, res, strict=False) # can contain _id
                if existingLoc.loc.coordinates == location.loc.coordinates:
                    assert existingLoc.lid == location.lid, 'same location different lid'
                else:
                    # new location
                    assert not location.HasField('lid'), 'new location has lid'
                    existingLoc.CopyFrom(location)
                    isNewLocation = True
            else:
                # new location
                assert not location.HasField('lid'), 'new location has lid'
                existingLoc.CopyFrom(location)
                isNewLocation = True

            # assign lid to new location
            if isNewLocation:
                existingLoc.lid = random.randint(0, sys.maxint)
                location.lid = existingLoc.lid
                existingLoc.ClearField('client_id')
                location.ClearField('client_id')
                existingLoc.computed_rating = 0
                location.computed_rating = 0

            # update location rating or title/location but not at the same time
            if location.HasField('user_rating'):
                # can't rate a new location, otherwise title/description won't be recorded.
                assert not isNewLocation
                # check if the user has rated this location before and remove that rating
                cursor = self.loc_hist_coll.find({'loc_id':location.lid, 'client_id':location.client_id}).sort('$natural', pymongo.DESCENDING).limit(1)
                
                while (yield cursor.fetch_next):
                    # should only iterate once
                    res = cursor.next_object()
                    hist = dict_to_protobuf(History, res, strict=False)
                    if hist.rating == location.user_rating:
                        # same rating as before
                        self.write({'ok': 1})
                        return
                    # remove the old rating
                    old_rating_type = SubmissionHandler.combined_rating_enum[hist.rating]
                    old_rating_count = getattr(existingLoc.combined_rating, old_rating_type)
                    setattr(existingLoc.combined_rating, old_rating_type, old_rating_count - 1)
                    break

                # add the new rating
                rating_type = SubmissionHandler.combined_rating_enum[location.user_rating]
                cur_rating_count = getattr(existingLoc.combined_rating, rating_type)
                setattr(existingLoc.combined_rating, rating_type, cur_rating_count + 1)
                existingLoc.computed_rating = self.compute_rating(existingLoc.combined_rating)
            else:
                if location.HasField('title'):
                    existingLoc.title = location.title
                if location.HasField('description'):
                    existingLoc.description = location.description

            existingLoc.version += 1
            op1 = self.loc_info_coll.update({'lid': existingLoc.lid}, protobuf_to_dict(existingLoc), upsert=True)
            op2 = self.store_loc_history(location)
            yield [op1, op2]

            self.write({'ok': 1, 'lid': existingLoc.lid})

        except AssertionError as e:
            # 499 is location not in NYC
            print e
            self.write_error(400, message=str(e))
        except Exception as e:
            print e
            self.write_error(400, message=str(e))

