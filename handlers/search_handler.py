# Tkaes in a coordinate or an address string, plus a radius
# Returns the viewport center and the list of toilets within the radius
# Also a short description of each location. (limit to 10 higest rated or something)

# uses the rr.info collection
import pymongo

from tornado import gen
from protobuf_to_dict import dict_to_protobuf

from handlers.base_handler import BaseHandler
from model.location_pb2 import Location, Locations

class SearchHandler(BaseHandler):
    
    @gen.coroutine
    def get(self):
        locations = Locations()

        lat = self.get_argument("lat")
        lon = self.get_argument("long")
        # radius in meters
        r = self.get_argument("radius")

        query = { 'loc': {
            '$nearSphere': {
                '$geometry': {'type': 'Point',
                    'coordinates': [float(lon), float(lat)]
                    },
                '$maxDistance': float(r)
                }
            },
            'computed_rating': {'$gte': -0.8}
        }

        cursor = self.loc_info_coll.find(query).sort('computed_rating', pymongo.DESCENDING).limit(20)

        while (yield cursor.fetch_next):
            obj = cursor.next_object()
            loc = locations.locations.add()
            dict_to_protobuf(loc, obj, strict=False)
        self.write(locations.SerializeToString())
