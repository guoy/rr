import json
import tornado.web

from tornado.web import RequestHandler


class BaseHandler(RequestHandler):

    def initialize(self, db):
        self.db = db
        self.loc_info_coll = db.loc_info
        self.loc_hist_coll = db.loc_hist
        self.set_header('Content-Type', 'application/x-protobuf')


    def write_error(self, status_code, **kwargs):
        if 'message' not in kwargs:
            if status_code == 405:
                kwargs['message'] = 'Invalid HTTP method.'
            else:
                kwargs['message'] = 'Unknown error.'

        self.set_status(status_code)
        self.write(kwargs['message'])