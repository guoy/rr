from tornado import gen

from handlers.base_handler import BaseHandler


class DummyHandler(BaseHandler):
    def initialize(self, db):
        super(DummyHandler, self).initialize(db=db)
        self.add_header("Cache-Control", "no-cache, must-revalidate")
    
    @gen.coroutine
    def get(self):
        self.write({"get":"1"})
        
    @gen.coroutine
    def post(self):
        self.write({"post":"1"})
