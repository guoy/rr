# setting up the server for the first time

import sys
import pymongo


if sys.platform == 'darwin':
    # TODO, we should really use a mock db here.
    CONN_STR = 'mongodb://localhost:27017'
    db = pymongo.MongoClient(CONN_STR).rr
    # do not drop the wrong db
    pymongo.MongoClient(CONN_STR).drop_database('rr')

else:
    CONN_STR = 'mongodb://storage-1:27017,storage-2:27017,app-1:27017/?replicaSet=rr'
    db = pymongo.MongoClient(CONN_STR).rr

db.loc_info.create_index([('loc', '2dsphere')])
db.loc_info.create_index([('computed_rating', 1)])
db.loc_hist.create_index([('loc_id', 1), ('client_id', 1)])