
import json

from tornado import testing


from tornado.testing import AsyncHTTPTestCase
from protobuf_to_dict import dict_to_protobuf


from model.location_pb2 import Location, Locations, RATING_POSITIVE, RATING_NEUTRAL, RATING_NEGATIVE

from server import RestroomApplication


# inits the server and db
import init_server

class SubmissionHandlerTest(AsyncHTTPTestCase):
    def get_app(self):
        return RestroomApplication(True)

    def test_normal(self):
        loc_dict = {
            "loc": {
                "type": "Point",
                "coordinates": [-73.972772, 40.789276]
            },
            "title": "normal location",
        }

        loc = Location()
        dict_to_protobuf(loc, loc_dict)
        res = self.fetch('/submit', method='POST', body=loc.SerializeToString())
        assert res.body is not None
        assert res.error is None

    def test_rating(self):
        loc_dict = {
            "client_id": "001",
            "loc": {
                "type": "Point",
                "coordinates": [-73.9727721, 40.7892761]
            },
            "title": "rating location",
            "user_rating": RATING_POSITIVE,
        }

        loc = Location()
        dict_to_protobuf(loc, loc_dict)
        res = self.fetch('/submit', method='POST', body=loc.SerializeToString())
        assert res.body is not None
        assert res.error is None
        res = json.loads(res.body)
        lid = res['lid']

        res = self.fetch('/search/?lat=40.76&long=-74&radius=100000000')
        locs = Locations.FromString(res.body)
        assert abs(locs.locations[0].computed_rating - 0.5) < 0.01

        loc_dict = {
            "client_id": "001",
            "lid": lid,
            "loc": {
                "type": "Point",
                "coordinates": [-73.9727721, 40.7892761]
            },
            "title": "rating location",
            "user_rating": RATING_NEGATIVE,
        }

        loc = Location()
        dict_to_protobuf(loc, loc_dict)
        res = self.fetch('/submit', method='POST', body=loc.SerializeToString())
        assert res.body is not None
        assert res.error is None

        res = self.fetch('/search/?lat=40.76&long=-74&radius=100000000')
        locs = Locations.FromString(res.body)
        assert abs(locs.locations[0].computed_rating + 0.5) < 0.01

        loc_dict = {
            "client_id": "002",
            "lid": lid,
            "loc": {
                "type": "Point",
                "coordinates": [-73.9727721, 40.7892761]
            },
            "title": "rating location",
            "user_rating": RATING_NEGATIVE,
        }

        loc = Location()
        dict_to_protobuf(loc, loc_dict)
        res = self.fetch('/submit', method='POST', body=loc.SerializeToString())
        assert res.body is not None
        assert res.error is None

        res = self.fetch('/search/?lat=40.76&long=-74&radius=100000000')
        locs = Locations.FromString(res.body)
        assert abs(locs.locations[0].computed_rating + 0.704832764699) < 0.01

    def test_modify_title(self):
        loc_dict = {
            "loc": {
                "type": "Point",
                "coordinates": [-73.9727722, 40.7892762]
            },
            "title": "basic location",
        }

        loc = Location()
        dict_to_protobuf(loc, loc_dict)
        res = self.fetch('/submit', method='POST', body=loc.SerializeToString())
        assert res.body is not None
        assert res.error is None
        res = json.loads(res.body)
        lid = res['lid']

        loc_dict = {
            "lid": lid,
            "loc": {
                "type": "Point",
                "coordinates": [-73.9727722, 40.7892762]
            },
            "title": "basic location updated",
        }

        loc = Location()
        dict_to_protobuf(loc, loc_dict)
        res = self.fetch('/submit', method='POST', body=loc.SerializeToString())
        assert res.body is not None
        assert res.error is None

        loc_dict = {
            "lid": lid,
            "loc": {
                "type": "Point",
                "coordinates": [-73.9727722, 40.7892762]
            },
            "title": "basic location updated again",
        }

        loc = Location()
        dict_to_protobuf(loc, loc_dict)
        res = self.fetch('/submit', method='POST', body=loc.SerializeToString())
        assert res.body is not None
        assert res.error is None

        res = self.fetch('/search/?lat=40.76&long=-74&radius=100000000')
        locs = Locations.FromString(res.body)
        loc = locs.locations[0]
        assert loc.title == 'basic location updated again'
        assert loc.version == 2


class SearchHandlerTest(AsyncHTTPTestCase):
    def get_app(self):
        return RestroomApplication(True)


class DummyHandlerTest(AsyncHTTPTestCase):
    def get_app(self):
        return RestroomApplication(True)

    def test_dummy(self):
        res = self.fetch('/', method='GET')
        assert res.body is not None
        assert res.error is None

if __name__ == '__main__':
    # Run this file with
    # python -m unittest discover tests 
    testing.main()