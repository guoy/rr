import json
import HTMLParser
import re
from bs4 import BeautifulSoup

import requests

from protobuf_to_dict import dict_to_protobuf

from model.location_pb2 import Location

allf = open('data/all.json')

for line in allf.readlines():
    d = json.loads(line)
    d['loc'] = {'type': 'Point', 'coordinates': [float(d['lon']), float(d['lat'])]}
    del d['lon']
    del d['lat']

    # d['lid'] = int(d['oid'])
    del d['oid']

    del d['category']

    d['title'] = d['name']
    del d['name']

    if 'address' in d and d['address'] != '':
        d['address'] = d['address'].title()
        d['description'] = 'Address: ' + d['address'] + '\n\n'
    else:
        d['description'] = ''

    if 'accessible' not in d:
        pass
    elif d['accessible'] == 'unknown' or d['accessible'] == '':
        del d['accessible']
    else:
        if d['accessible'] != 'no':
            d['accessible'] = True
        else:
            d['accessible'] = False
        d['description'] += 'Accessible: ' + str(d['accessible']) + '\n\n'
    
    if 'hours' not in d:
        pass
    elif d['hours'] == '':
        del d['hours']
    else:
        d['description'] += 'Hours: ' + d['hours'] + '\n'

    d['computed_rating'] = 0
    loc = Location()
    dict_to_protobuf(loc, d)
    # https://rr-app-1.eastus.cloudapp.azure.com
    r = requests.post('http://localhost:8994/submit', data=loc.SerializeToString(), headers={'Content-Type': 'application/x-protobuf'})
    if r.status_code != 200:
        print r.text
        print r.status_code


# coordf = open('coord.json')

# loc_dic = {}

# for line in coordf.readlines():
#     linedict = json.loads(line)
#     l = linedict['marker']
#     loc_dic[l['oid']] = l


# locf = open('locations.json')

# for line in locf.readlines():
#     linedict = json.loads(line)
#     if linedict['lid'] in loc_dic:
#         lid = linedict['lid']
#         loc = loc_dic[lid]
#         loc['accessible'] = linedict['accessible']
#         loc['name'] = linedict['title']
#         loc['category'] = linedict['type']
#         loc['hours'] = linedict['hours']
#         loc['address'] = linedict['address']

# outf = open('all.json', 'w')
# for k in loc_dic:
#     outf.write(json.dumps(loc_dic[k]))
#     outf.write('\n')







# outf = open('locations.json', 'w')

# for i in range(1, 137):
#     f = open('out/page' + str(i) + '.html')
#     s = BeautifulSoup(f, 'html.parser')
#     t1 = s.find(id="data-table")
#     t2 = t1.find('tbody')
#     t3 = t2.find_all('tr')
#     for row in t3:
#         t4 = row.find_all('td')
#         assert len(t4) == 8
#         lid = HTMLParser.HTMLParser().unescape(t4[0]).get_text()
#         loc_type = HTMLParser.HTMLParser().unescape(t4[3]).get_text().lower()
#         title = HTMLParser.HTMLParser().unescape(t4[2].get_text()).title()
#         address = HTMLParser.HTMLParser().unescape(t4[4].get_text()).lower()
#         address = address.split(',')[0]
#         address = address.replace('ny', '')
#         address = re.sub(r'\s+', ' ', address)
#         address = re.sub(r'\bst\.?\b', 'street', address)
#         address = re.sub(r'\bave\.?\b', 'avenue', address)
#         address = re.sub(r'\brd\.?\b', 'road', address)
#         address = re.sub(r'\bblvd\.?\b', 'boulevard', address)
#         address = re.sub(r'\be\.?\b', 'east', address)
#         address = re.sub(r'\bw\.?\b', 'west', address)
#         address = re.sub(r'\bn\.?\b', 'north', address)
#         address = re.sub(r'\bs\.?\b', 'south', address)
#         address = address.replace('.', '')
#         address = address.strip()

#         accessible = HTMLParser.HTMLParser().unescape(t4[6].get_text()).lower()
#         hours = HTMLParser.HTMLParser().unescape(t4[5].get_text()).lower()
#         outf.write(json.dumps({'lid': lid, 'type': loc_type, 'title': title, 'address': address, 'accessible': accessible, 'hours': hours}))
#         # outf.write(json.dumps({'address': address}))
#         outf.write('\n')

# f = open('data.json')
# outf = open('coord.json','w')
# j = json.loads(f.read())
# for l in j['marker_list']:
#     outf.write(json.dumps(l))
#     outf.write('\n')



