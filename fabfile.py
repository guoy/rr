import os, sys

from fabric.api import *


def dummy():
    cmd = "curl 'http://m3.mappler.net/dechr/class/list_data_web_fn.php?page={0}&blogid=nyrestroom' -H 'Cookie: PHPSESSID=0ec3e5hnt3ehb5pckgidij3fc2; _ga=GA1.2.1263962051.1460949444; _gat=1' -H 'Accept-Encoding: gzip, deflate, sdch' -H 'Accept-Language: en-US,en;q=0.8,zh-CN;q=0.6,zh;q=0.4' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36' -H 'Accept: text/html, */*; q=0.01'  -H 'X-Requested-With: XMLHttpRequest' -H 'Connection: keep-alive' --compressed"
    for i in range(1, 137):
        logs = local(cmd.format(str(i)), True)
        f = open('out/page' + str(i) + '.html', 'w')
        f.write(logs)
        f.close()
