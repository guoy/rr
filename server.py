import os
import platform
import sys

import motor
import tornado.httpserver
import tornado.ioloop
import tornado.options

from tornado.web import Application

from handlers import *


here = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, here)


DEFAULT_PORT = 8994

class RestroomApplication(Application):
    

    def __init__(self, for_test=False):

        if for_test or sys.platform == 'darwin':
            # TODO, we should really use a mock db here.
            CONN_STR = 'mongodb://localhost:27017'
            db = motor.MotorClient(CONN_STR).rr
            for_test = True
        else:
            CONN_STR = 'mongodb://storage-1:27017,storage-2:27017,app-1:27017/?replicaSet=rr'
            db = motor.MotorClient(CONN_STR).rr

        settings = dict(
            debug=for_test,
            autoreload=False,
            xsrf_cookies=False,
            static_path=os.path.join(here, 'static'),
        )
        
        paths = {"path": "./static"}

        handlers = [
            (r"/?", dummy_handler.DummyHandler, dict(db=db)),
            (r"/search/?", search_handler.SearchHandler, dict(db=db)),
            (r"/submit/?", submission_handler.SubmissionHandler, dict(db=db)),
        ]

        Application.__init__(self, handlers, **settings)


def main():
    tornado.options.parse_command_line()
    tornado.httpserver.HTTPServer(RestroomApplication(), xheaders=True).listen(DEFAULT_PORT)
    tornado.ioloop.IOLoop.current().start()


if __name__ == '__main__':
    main()